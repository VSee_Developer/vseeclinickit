//
//  VSeeChatListViewController.h
//  CEP
//
//  Created by Ken on 9/2/16.
//  Copyright © 2016 vsee. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SDWebImage;

@interface VSeeChatListViewController : UITableViewController

- (void)reloadChatControllersAfterTimeout;

@end
