//
//  VSeeNotificationCenterAdapter.h
//  FruitStreet
//
//  Created by Ken on 3/21/16.
//  Copyright © 2016 vsee. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VSeeNotificationCenterDelegate;

extern NSString *VSeeIncomingCallNotificationName;
extern NSString *VSeeIncomingChatNotificationName;

@interface VSeeNotificationCenterAdapter : NSObject

@property (nonatomic, weak) id<VSeeNotificationCenterDelegate> delegate; // delegate methods from VSeeNotificationCenter will be forwarded to this object

+ (instancetype)sharedAdapter; // singleton instance

- (void)retrieveUserImageForNotification:(UIViewController *)notificationController completion:(void (^)(UIImage *))completionHandler;
- (void)showChatList;

@end
