//
//  AppSettings.h
//  VSeeClinicKit
//
//  Created by Ken on 6/6/17.
//  Copyright © 2017 vsee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VSeeNetworkManagerAdapter.h"

@interface AppSettings : NSObject

@property (class, nonatomic, copy, readonly, nonnull) AppSettings *settings;

@property (nonatomic, copy, nonnull) NSString *appStoreName;
@property (nonatomic, copy, nonnull) NSString *appStoreId;
@property (nonatomic, copy, nonnull) NSString *hockeyAppId;
@property (nonatomic, copy, nonnull) NSString *conversionId;
@property (nonatomic, copy, nonnull) NSString *conversionDownloadLabel;
@property (nonatomic, copy, nonnull) NSString *conversionScheduleAppointmentLabel;
@property (nonatomic, copy, nonnull) NSString *conversionSignUpLabel;

@property (nonatomic, copy, readonly, nonnull) NSString *apiKey;
@property (nonatomic, copy, readonly, nonnull) NSString *networkBaseUrl;
@property (nonatomic, assign) NSTimeInterval scheduledVisitMaxWaitTime;
@property (nonatomic, assign, getter=isRescheduleAppointmentAllowed) BOOL rescheduleAppointmentAllowed;
@property (nonatomic, assign, getter=isGuestLoginEnabled) BOOL guestLoginEnabled;
@property (nonatomic, assign, getter=isGuestLoginOnly) BOOL guestLoginOnly;
@property (nonatomic, assign) VSeeLoginSettingsAdapter vseeLoginSettings;

+ (void)registerSettingsClass:(nonnull Class)settingsClass;

- (void)updateSettings:(nonnull NSDictionary *)json;

@end
