//
//  HealthDataConfig.h
//  VSeeClinicKit
//
//  Created by Ken Tran on 5/10/17.
//  Copyright © 2017 vsee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthDataConfig : NSObject

@property (assign, nonatomic, readonly) BOOL weightEnabled;
@property (assign, nonatomic, readonly) BOOL stepsEnabled;
@property (assign, nonatomic, readonly) BOOL caloriesEnabled;
@property (assign, nonatomic, readonly) BOOL sleepEnabled;
@property (assign, nonatomic, readonly) BOOL activeMinutesEnabled;
@property (assign, nonatomic, readonly) BOOL bloodGlucoseEnabled;
@property (assign, nonatomic, readonly) BOOL bloodPressureEnabled;
@property (assign, nonatomic, readonly) BOOL heartRateEnabled;

- (instancetype)initWithJson:(NSDictionary *)json;

@end
