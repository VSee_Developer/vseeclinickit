//
//  VSeeChatViewManager+Private.h
//  VSeeClinicKit
//
//  Created by Ken on 11/7/16.
//  Copyright © 2016 vsee. All rights reserved.
//

#import <Foundation/Foundation.h>
@import VSeeKit;

@interface VSeeChatViewManager (Private)

// expose parts of the private APIs for public use
// directly importing "VSeeChatViewManager_Private.h" will cause symbol duplication error

- (BOOL)restoreActiveChats;
- (UIViewController<VSeeChatViewControllerProtocol> *)activeChatController;

@end
