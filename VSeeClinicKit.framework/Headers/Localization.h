//
//  Localization.h
//  VSeeClinicKit
//
//  Created by Ken Tran on 17/4/17.
//  Copyright © 2017 vsee. All rights reserved.
//


#ifndef Localization_h
#define Localization_h

#import <Foundation/Foundation.h>

NSString * EVLocalizedString(NSString *key, NSString *comment);

#endif /* Localization_h */
