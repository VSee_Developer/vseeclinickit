//
//  VSeeChatViewManager+Utilities.h
//  FruitStreet
//
//  Created by Ken on 3/18/16.
//  Copyright © 2016 vsee. All rights reserved.
//

#import <VSeeKit/VSeeChatViewManager.h>

@interface VSeeChatViewManager (Utilities)

- (UIViewController<VSeeChatViewControllerProtocol> *)oneToOneChatControllerWithVSeeId:(NSString *)vseeid;

@end
